import { Component, OnInit } from '@angular/core';
import { NavController, NavParams, ToastController } from 'ionic-angular';
// import { Http, Headers } from '@angular/http';
import { Subscription } from 'rxjs/Subscription';
import "rxjs/add/observable/interval";
import { ApiProvider } from '../../providers/api/api';
import { Observable } from 'rxjs/Observable';
import { UserEditPage } from '../user-edit/user-edit';
import { Storage } from "@ionic/storage";
/**
 * Generated class for the UserAuthPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-user-auth',
  templateUrl: 'user-auth.html',
})
export class UserAuthPage {
  user: any;
  code = {
    nb1: "",
    nb2: "",
    nb3: "",
    nb4: ""
  };
  delai: any;
  secondes: number;
  minutes: number;
  btn = "Entrez votre code";
  spinner: boolean = false;
  headers = new Headers();
  timer: Subscription;
  tempRecu: boolean = false;
  rebours: boolean = true;
  direction_id = null;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private toastCtrl: ToastController,
    public apiService : ApiProvider,
    private  store : Storage
  ) {
    this.headers.append("Content-type", "application/json");
  }
  moveFocus(nextElement) {
    nextElement.setFocus();
  }
  ngOnInit() {
    const count = Observable.interval(1000);
    console.log(this.navParams.get("value"));
    if (this.navParams.get("value") !== undefined) {
      let id =this.navParams.get("value")
      this.apiService
        .requeteGet(id,'getsecondes')
        .then(val => {
          if (val == null) {
            //console.log("temps déjà terminé");
            this.rebours = false;
          } else {
            if (val !== undefined) {
              this.delai = val;
              this.tempRecu = true;
              this.minutes = Math.floor(this.delai / 60);
              this.secondes = Math.floor(this.delai % 60);
              this.timer = count.subscribe(val => {
                if (this.minutes >= 1) {
                  if (--this.secondes < 0) {
                    this.secondes = 59;
                    --this.minutes;
                  }
                } else {
                  this.minutes = 0;
                  if (this.secondes > 0) {
                    if (--this.secondes < 0) {
                      this.secondes = 0;
                      if (this.secondes == 0) {
                        this.rebours = false;
                      }
                    }
                  } else {
                    this.secondes = 0;
                  }
                }
              });
            }
          }
        });
    }
  }

  toast = this.toastCtrl.create({
    message: "Compte activé avec succès",
    duration: 3000,
    position: "top"
  });
  getUserByCode(next) {
    this.moveFocus(next);
    let code =
      String(this.code.nb1) +
      String(this.code.nb2) +
      String(this.code.nb3) +
      String(this.code.nb4);
    let body = { code: code, direction_id: this.direction_id };
    if (
      String(this.code.nb1) != "" &&
      String(this.code.nb2) != "" &&
      String(this.code.nb3) != "" &&
      String(this.code.nb4) != ""
    ) {
      this.btn = "Validation en cours...";
      this.spinner = true;
      this.apiService
        .requeteGet(code,'authByCode')
        .then(
          resolve => {
            console.log("en dehors");
            if (resolve != null) {
              this.btn = "Validation réussie";
              this.user = resolve;
              console.log(this.user);
              this.store.set("userkey", this.user).then(result => {
                this.navCtrl.setRoot(UserEditPage)
                // this.toast.present().then(() => {
                // this.navCtrl.setRoot(UserEditPage);
                this.presentToast(
                  "Bienvenue cher Collaborateur !",
                  "bottom",
                  3000
                );
                // });
              });
            } else {
              this.presentToast(
                "Code Invalide, merci de réessayez",
                "bottom",
                3000
              );
              this.btn = "Réessayez";
              this.spinner = false;
              this.code = {
                nb1: "", nb2: "", nb3: "", nb4: ""
              }
            }
          },
          reject => {
            console.log("Raison du rejet " + reject);
            this.btn = "Réessayez";
            this.spinner = false;
            this.code = {
              nb1: "", nb2: "", nb3: "", nb4: ""
            }
          }
        )
        .catch(err => {
          this.presentToast(
            "Requete non envoyée, probleme serveur",
            "bottom",
            3000
          );
          console.log("Requete non envoyée " + err);
          this.btn = "Réessayez";
          this.spinner = false;
          this.code = {
            nb1: "", nb2: "", nb3: "", nb4: ""
          }
        });
    }
  }
  
  presentToast(msg: string, position: string, delay: number) {
    let toastfunction = this.toastCtrl.create({
      message: msg,
      duration: delay,
      position: position
    });

    toastfunction.onDidDismiss(() => {
      console.log("Dismissed toast");
    });

    toastfunction.present();
  }
}
