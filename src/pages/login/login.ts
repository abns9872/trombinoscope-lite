import { Component } from '@angular/core';
import { NavController, NavParams, LoadingController, ToastController } from 'ionic-angular';
import { ApiProvider } from '../../providers/api/api';
import { HomePage } from '../home/home';
import { UserAuthPage } from '../user-auth/user-auth';
import { Storage } from '@ionic/storage';
import { UserEditPage } from '../user-edit/user-edit';
/**
 * Generated class for the LoginPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

// @IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {
  user = { email:'',password:''};
  auth :any;
  loader : any;
  test : boolean = false;
  constructor(public navCtrl: NavController, public navParams: NavParams,
    public loadingCtrl: LoadingController,public toastCtrl: ToastController,
     public api: ApiProvider,private store:Storage) {
    this.store.get('user').then((data) => {
      if (data != null) {
        this.navCtrl.setRoot(HomePage);
      }
      else {

      }
    })
  }
  // ionViewDidLoad() {
  //   ////console.log('ionViewDidLoad LoginPage');

  // }
  login(form){
    console.log(this.user);
    this.presentLoading();
        this.api.getConnexion(this.user).then(
          (res) => {
            this.loader.dismiss();
            let u = res;
            if (res != null && res['id']){
              this.store.set("userkey", u).then(result => {
                this.navCtrl.setRoot(UserEditPage);
              });
            }
            else if (res == "erreur_email"){
              this.errorLogin();
            }
            else if (res == "erreur_serveur"){
              this.errordb();
            }
            else if (res == "erreur_email"){
              this.errorLogin();
            }
            else{
              this.status500Toast();
            }
          },
          (err)=>{
            this.status500Toast();
          }
        );
  }
  presentLoading() {
    this.loader = this.loadingCtrl.create({
      content: "Connexion ...",
      // duration: 5000
    });
    this.loader.present();
  }
  Erreurcode() {
    // this.loader.dismiss();
    const toast = this.toastCtrl.create({
      message: 'email doit être égal à 8 caractères',
      duration: 5000
    });
    toast.present();
  }
  status500Toast() {
    // this.loader.dismiss();
    const toast = this.toastCtrl.create({
      message: 'Login ou mot de passe incorrect',
      duration: 5000
    });
    toast.present();
  }
  showLoader() {
    this.loader = this.loadingCtrl.create({
      content: "Please wait...",
      // duration: 1000
    });
    this.loader.present();
    // this.navCtrl.setRoot(HomePage)

  }
  errorLogin() {
    const toast = this.toastCtrl.create({
      message: 'Code email incorrect',
      duration: 6000
    });
    toast.present();
  }
  errordb() {
    const toast = this.toastCtrl.create({
      message: 'Accès à la base de données refusée',
      duration: 6000
    });
    toast.present();
  }
  signIn(form){
    ////console.log(this.user);
    this.presentLoading();
    if (form.value.email != '' && form.value.password != ''){
      this.api.getConnexion(this.user).then(
        (res)=>{
          ////console.log(res);
          this.auth = res;
          if (this.auth == null) {
            // this.loader.dismiss();
            ////console.log('status 500');
            this.status500Toast();
            // this.loader.dismiss();

          }
          else {
            this.loader.dismiss();
            if (this.auth['rep'] == "success") {
              this.navCtrl.setRoot(HomePage);
            }
            else if (this.auth['rep'] == "error") {
              ////console.log(this.auth);
              this.errorLogin();
            }
            else {
              ////console.log(this.auth);
              this.errordb();
            }
          }
          // if(this.auth['rep'] == 'success'){
          //   this.loader.dismiss();
          //   this.navCtrl.setRoot(HomePage);
          // }
        }
      )
    }
  }
}
