import { Component } from '@angular/core';
import { NavController, NavParams, ToastController, LoadingController } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { Http, Headers } from '@angular/http';
import { ApiProvider } from '../../providers/api/api';
import { Camera } from '@ionic-native/camera';
import { File, FileEntry } from '@ionic-native/file';
/**
 * Generated class for the UserProfilPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-user-profil',
  templateUrl: 'user-profil.html',
})
export class UserProfilPage {
  collaborateur = {
    nom: '',
    igg:'',
    prenom: '',
    adresse: '',
    poste: '',
    telephone: '',
    telephonefixe: '',

    };
  mypic:any;
  agents: any;
  user: any;
  directions: any;
  public loading;
  public myPhoto: any;
  public urlImg: any;
  direction: any = null;
  directionId: any = null;
  ready: boolean = false;
  spinner: boolean = false;
  urlRecu: any = false;
  formData = new FormData();
  headers = new Headers();
  userId: number;
  btn: string='Modifier';
  disable : boolean = true;
  baseUrl: string = "http://dev-trombino.afridev-groupe.com/api";
  constructor(public navCtrl: NavController,
      public toastCtrl: ToastController,
      public apiService: ApiProvider, public navParams: NavParams,
      public store: Storage, private http: Http,
      private file: File,public loadingCtrl : LoadingController,
      private camera: Camera,
) {
      this.headers.append("Content-type", "application/json");
      this.file
      .checkDir(this.file.dataDirectory, "myDir")
      .then(_ => console.log("Directory exists"))
      .catch(err => console.log("Directory doesn't exist"));
  }
  enable(){
    this.disable =false;
    this.btn = 'Valider';
  }
  errorsave() {
    const toast = this.toastCtrl.create({
      message: 'l enregistrement à échoué',
      duration: 6000
    });
    toast.present();
  }
  errordb() {
    const toast = this.toastCtrl.create({
      message: 'Accès à la base de données refusée',
      duration: 6000
    });
    toast.present();
  }
  success() {
    const toast = this.toastCtrl.create({
      message: 'Mise à jour reussie',
      duration: 6000
    });
    toast.present();
  }
 
  ionViewDidLoad() {
    this.store.get('utilisateur').then(res => {
      if (res) {
        this.collaborateur = res;
        console.log('id collaborateur')
        this.userId = this.collaborateur['id'];
        if (this.collaborateur['photo'] != null) {
          this.urlRecu = "http://dev-trombino.afridev-group.com/img/" + res.photo;
        }
      }
    })
    //console.log('ionViewDidLoad UserProfilPage');
  }
  updateUser(data) {
    this.spinner = true;
    this.apiService.updateUsers(this.collaborateur).then(
      (res) => {
        //console.log(res);
        this.spinner = false;
        if (res != null && res != "erreur_serveur") {
          this.agents = this.store.set('utilisateur', this.collaborateur).then(
            (res) => {
              // this.store.remove('utilisateur');
              console.log('nouveau agent');

              // this.store.set('utilisateur', res);
              this.success()
            });
        }
        else if (res == "erreur_serveur") {
          this.errorsave();
        }
        else {
          this.errordb();
        }
      },
      (err) => {

      }
    )
  }
  fromGallery(): void {
    this.camera
      .getPicture({
        sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
        destinationType: this.camera.DestinationType.FILE_URI,
        quality: 80,
        targetWidth: 360,
        targetHeight: 360,
        encodingType: this.camera.EncodingType.PNG,
        allowEdit: true,
        saveToPhotoAlbum: true
      })
      .then(
        imageData => {
          this.myPhoto = imageData;
          this.urlImg = "data:image/jpeg;base64," + imageData;
          this.uploadPhoto(imageData);
        },
        error => {
          console.log(JSON.stringify(error));
        }
      );
  }
  private uploadPhoto(imageFileUri: any): void {
    this.showLoader("Mise à jour...");
    this.file
      .resolveLocalFilesystemUrl(imageFileUri)
      .then(entry => (<FileEntry>entry).file(file => this.readFile(file)))
      .catch(err => {
        console.log(err);
        this.presentToast(
          "Error Directory !",
        )
      });
  }

  private readFile(file: any) {
    const reader = new FileReader();
    console.log("reading file", file);
    reader.onloadend = () => {
      // const formData = new FormData();
      const imgBlob = new Blob([reader.result], { type: file.type });
      this.formData.append("image", imgBlob, file.name);
      if (this.formData != undefined) {
        this.editImg();
      }
    };
    reader.readAsArrayBuffer(file);
  }

  editImg() {
    this.uploadImage(this.formData, this.userId)
      .then(
        val => {
          this.dismissLoader();
          console.log('Edit image');
          if (val != null) {
            console.log("newAgent")
            // this.store.remove('utilisateur');
            this.store.set('utilisateur', val);
            this.store.set('userkey', val);
            this.urlRecu = 'http://dev-trombino.afridev-group.com/img/' + val['photo'];
            console.log(this.urlRecu);
            this.presentToast(
              "Photo mise à jour avec succés !",
            );
            this.dismissLoader();
          } else {
            this.presentToast(
              "Echec Upload fichier : "
            );
            // this.dismissLoader();
          }
        },
        err => {
          console.log("Erreur : " + err);
          this.dismissLoader();
          this.presentToast(
            "Erreur Connexion internet !",
          );
        }
      )
      .catch(err => {
        console.log("Erreur :" + err);
        // this.dismissLoader();
      });
  }

  private uploadImage(formData, userId) {
    return new Promise((resolve, reject) => {
      this.post(
        "setimage/" + userId + ".json",
        { user_id: userId },
        formData
      ).subscribe(
        response => {
          resolve(response.json());
        },
        err => {
          this.presentToast(
            "Erreur upload image !",
          );
          console.log(err);
          reject(err);
        }
      );
    });
  }
  private post(endpoint: string, params: any = null, body: any = null) {
    return this.http.post('http://dev-trombino.afridev-group.com/api/users/' + endpoint, body, {
      params: params
    });
  }
  private showLoader(message: string = "loading") {
    this.loading = this.loadingCtrl.create({
      content: message,
      // duration: 3000
    });

    this.loading.present();
  }
  private dismissLoader() {
    if (this.loading) {
      this.loading.dismiss();
      this.loading = null;
    }
  }
  presentToast(msg) {
    const toast = this.toastCtrl.create({
      message: msg,
      duration: 3000
    });
    toast.present();
  }
  
}