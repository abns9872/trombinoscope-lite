import { Component } from '@angular/core';
import { NavController, NavParams, AlertController } from 'ionic-angular';
import { DirectionPage } from '../direction/direction';
import { UserProfilPage } from '../user-profil/user-profil';
import { Storage } from '@ionic/storage';
import { ApiProvider } from '../../providers/api/api';

/**
 * Generated class for the AgentPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

// @IonicPage()
@Component({
  selector: 'page-agent',
  templateUrl: 'agent.html',
})
export class AgentPage {
  ROOT_IMG = 'http://dev-trombino.afridev-group.com/img/';
  agent: any;
  urlRecu: any = false;
  urlRecu1: any = false;
  test : boolean = false;
  user:any;
  constructor(public navCtrl: NavController, public navParams: NavParams,
    public apiService : ApiProvider,
    public alertCtrl: AlertController, public store: Storage,) {
    this.agent = this.navParams.get('value');
    ;
  }
  ionViewWillEnter() {
    this.store.get('utilisateur').then(
      res => {
        if (res) {
          this.user = res;
          if (this.user.photo != null) {
            this.urlRecu = "http://dev-trombino.afridev-group.com/img/" + this.user.photo
          }
        }
        this.apiService.updateSession(this.user.email)
          .then(
            res => {
              console.log('update session');
              this.store.set('utilisateur', res);
            },
            err => { }
          )
          .catch(
            error => {

            }
          )
      }
    );
  }
  ionViewDidLoad() {
  }
  goback() {
    this.navCtrl.setRoot(DirectionPage);
  }
  showPhone(){
    this.test = !this.test;
  }
  showAlert(tel1, tel2) {
    const alert = this.alertCtrl.create({
      title: 'Appeler',
      // subTitle: 'Your friend, Obi wan Kenobi, just accepted your friend request!',
      // inputs: [
      //   {
      //     type: 'password',
      //     name: 'code'
      //   }
      // ],

      buttons: [
        {
          text: tel1,
          cssClass:'alert-button',

        },
        {
          text: tel2,
          cssClass: 'alert-button'

        }
      ]
    });
    alert.present();
  }
  page(){
    this.navCtrl.push(UserProfilPage);
  }
}
