import { Component } from '@angular/core';
import { NavController, LoadingController  } from 'ionic-angular';
import { ApiProvider } from '../../providers/api/api';
import { DirectionPage } from '../direction/direction';
import { Storage } from "@ionic/storage";
import { AgentPage } from '../agent/agent';
import { UserProfilPage } from '../user-profil/user-profil';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  urlRecu: any = false;
  urlReculogo: any = false;
  agents: any;
  listedirections: any;
  services : any;
  pet: string = "Annuaire";
  searchQuery: string = '';
  items = [];
  itemagent = [];
  arrayAgent = [];
  filt: string;
  loader : any;
  user: any;
  nom : string;
  prenom: string;
  poste : string;
  agent = [];
  spiner: boolean = false;
  test: boolean = false;
  directeur = [];
  eid;
  constructor(
    public navCtrl: NavController,
    public apiService : ApiProvider,
    public loadingCtrl: LoadingController,
    private store : Storage
    )
    {}
    ionViewDidLoad(){
      console.log('ionViewDidLoad Page Home');

      this.store.get('userkey').then(res => {
        this.eid = res['entreprise_id'];
        this.urlReculogo = "http://dev-trombino.afridev-group.com/img/" + res['logo'];
        // console.log("entreprise id");
        console.log(this.urlReculogo);
        this.ionViewWillEnter();
      });
    }
    ionViewWillEnter() {
      console.log('ionViewWillEnter Page Home');
      // console.log(this.eid);

      this.apiService._showlistservice(this.eid).then(res=>{
        this.agent = res['agents'];
        this.directeur= res['directeurs'];
        this.agent.forEach(el=>{
          this.itemagent.push(el);
        })
        this.directeur.forEach(item=>{
          this.itemagent.push(item);
        })
      },
      (err)=>{

      }
      ).catch();
      this.store.get('utilisateur').then(
        res => {
          if (res) {
            let user = res;
            // console.log(user.photo)
            if (user.photo != null) {
              this.urlRecu = "http://dev-trombino.afridev-group.com/img/" + user.photo;
            }
          }
      });
      // console.log(this.items)
      this._direction(this.eid);
      this.initializeItems();

    }
  page() {
    this.navCtrl.push(UserProfilPage);
  }
  presentLoading() {
    this.loader = this.loadingCtrl.create({
      content: "Please wait...",
      // duration: 3000
    });
    this.loader.present();
  }
  doRefresh(refresher) {

    //console.log('Begin async operation', refresher);

    setTimeout(() => {
      ////console.log('Async operation has ended');
      refresher.complete();
    }, 2000);
  }
  _direction(eid){
    // this.presentLoading();
    this.apiService._showlistedirection(eid)
      .then(
        res=>{
          // this.loader.dismiss();
          //console.log(res['direction']);
          if (res['direction']){

            this.listedirections = res['direction'];
          }
          //console.log(this.listedirections);
        },
        err=>{
          //console.log('erreur serveur');
        }
      ).catch(
        error =>{
          this.apiService._showlistedirection(eid).then(res=>{
            //console.log(res['direction']);
            if (res['direction']) {
              this.listedirections = res['direction'];
            }
            //console.log(this.listedirections);
          },err=>{

          })
        }
      )
  }

  initializeItems() {
    this.test = false;
    this.items = []
    this.items = this.itemagent;
  }
  getItems(ev: any) {
    // Reset items back to all of the items
    this.initializeItems();

    // set val to the value of the searchbar
    const val = ev.target.value;
    ////console.log(val)
    // if the value is an empty string don't filter the items
    if (val && val.trim() != '') {
      ////console.log(this.items);
      this.test = true;
      this.spiner =true;
      this.items = this.items.filter((item) => {
        this.spiner =false;
        const value = item['nom']+' '+item['prenom']+' '+item['telephone'];
        return (value.toLowerCase().indexOf(val.toLowerCase()) > -1);
      })
    }
  }
  viewContact(id){
    ////console.log(id);
    // this.navCtrl.setRoot(AgentPage, { 'value': id })
    this.navCtrl.push(DirectionPage, { 'value': id })
  }
  gopage(id) {
    console.log(id);
    this.navCtrl.push(AgentPage, { 'value': id });
  }
}
