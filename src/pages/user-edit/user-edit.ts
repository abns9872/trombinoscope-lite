import { Component } from '@angular/core';
import { NavController, NavParams, ToastController, AlertController, LoadingController } from 'ionic-angular';
import { Storage } from "@ionic/storage";
import { ApiProvider } from '../../providers/api/api';
import { HomePage } from '../home/home';
import { Camera } from '@ionic-native/camera';
import { File, FileEntry } from '@ionic-native/file';
import { Http } from '@angular/http';
import { Push, PushObject, PushOptions } from '@ionic-native/push';
import { AgentPage } from '../agent/agent';

/**
 * Generated class for the UserEditPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-user-edit',
  templateUrl: 'user-edit.html',
})
export class UserEditPage {
  agents : any;
  agent = [];
  mypic: any;
  user: any;
  directions: any;
  public loading;
  public myPhoto: any;
  public urlImg: any;
  direction: any = null;
  directionId: any = null;
  ready: boolean = false;
  spinner: boolean = false;
  urlRecu: any = false;
  formData = new FormData();
  headers = new Headers();
  userId: number;
  spinner2: boolean =  false;
  btn: string = 'Modifier';
  disable: boolean = true;
  baseUrl: string = "http://dev-trombino.afridev-groupe.com/api";
  btnMaj : boolean = false;
  constructor(public navCtrl: NavController, public navParams: NavParams, private store: Storage,
    public apiService : ApiProvider, public toastCtrl: ToastController,public alertCtrl : AlertController,
    private file: File, public loadingCtrl: LoadingController,
      private push: Push,
    private camera: Camera, private http: Http,
    ) {
    this.headers.append("Content-type", "application/json");
    this.file
      .checkDir(this.file.dataDirectory, "myDir")
      .then(_ =>
        console.log("Directory exists"))
      .catch(err =>
         console.log("Header : Directory doesn't exist")
         );
  }
  ionViewWillEnter() {
    this.agents = this.store.get('userkey').then(
      res => {
        //console.log(res);
        this.agent = res;
        this.userId = this.agent['id'];
        this.saveRegister(this.userId);
        this.btnMaj = true;

        //console.log("userID appComponent: " + this.userId);
        if (this.agent['photo'] != null) {
          this.urlRecu = "http://dev-trombino.afridev-group.com/img/" + this.agent['photo'];
          //console.log(this.urlRecu)
        }
      }

    );
  }
  // ionViewDidLoad() {
    //console.log('ionViewDidLoad UserEditPage');

    //console.log(this.agents);

    //console.log(this.agent);

  // }

  updateUser(data){
    this.spinner2 = true;
    this.apiService.updateUsers(this.agent).then(
      (res)=> {
        console.log('chargement page home');
        if(res != null && res !="erreur_serveur"){
          this.agents = this.store.set('utilisateur',res).then(
            (res) => {
              if(res != null){
                // this.store.remove('user');
                // this.store.set('user',res);
                this.store.set('active','valide');
                this.navCtrl.setRoot(HomePage);
              }
          });
        }
        else if (res == "erreur_serveur"){
          this.errorsave();
        }
        else{
          this.errordb();
        }
      },
      (err)=>{

      }
    )
  }
  errordb() {
    const toast = this.toastCtrl.create({
      message: 'Echec modification',
      duration: 6000
    });
    toast.present();
  }
  errorsave() {
    const toast = this.toastCtrl.create({
      message: 'l enregistrement à échoué',
      duration: 6000
    });
    toast.present();
  }
  fromGallery(): void {
    this.camera
      .getPicture({
        sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
        destinationType: this.camera.DestinationType.FILE_URI,
        quality: 80,
        targetWidth: 360,
        targetHeight: 360,
        encodingType: this.camera.EncodingType.PNG,
        allowEdit: true
      })
      .then(
        imageData => {
          this.myPhoto = imageData;
          this.urlImg = "data:image/jpeg;base64," + imageData;
          this.uploadPhoto(imageData);
        },
        error => {
          console.log(JSON.stringify(error));
        }
      );
  }
  private uploadPhoto(imageFileUri: any): void {
    this.showLoader("Mise à jour...");
    this.file
      .resolveLocalFilesystemUrl(imageFileUri)
      .then(entry => (<FileEntry>entry).file(file => this.readFile(file)))
      .catch(err => {
        console.log(err);
        this.dismissLoader();
        this.presentToast(
        "Error Directory !",
        )
      });
  }

  private readFile(file: any) {
    const reader = new FileReader();
    console.log("reading file", file);
    reader.onloadend = () => {
      // const formData = new FormData();
      const imgBlob = new Blob([reader.result], { type: file.type });
      this.formData.append("image", imgBlob, file.name);
      if (this.formData != undefined) {
        this.editImg();
      }
    };
    reader.readAsArrayBuffer(file);
  }

  editImg() {
    this.uploadImage(this.formData, this.userId)
      .then(
        val => {
          this.dismissLoader();
          console.log('Edit image');
          console.log(val['photo'])
          if (val != null) {
            console.log('newAgent')
            this.store.set('userkey', val);
            this.urlRecu = 'http://dev-trombino.afridev-group.com/img/' + val['photo'];
            this.agent['photo'] = val['photo'];
            console.log(this.urlRecu);
            this.presentToast(
              "Photo mise à jour avec succés !",
            );
            // this.dismissLoader();
          } else {
            this.presentToast(
              "Echec Upload fichier : "
            );
            // this.dismissLoader();
          }
        },
        err => {
          console.log("Erreur : " + err);
          this.presentToast(
            "Erreur de chargement : "
          );
          // this.dismissLoader();
        }
      )
      .catch(err => {
        console.log("Erreur :" + err);
        this.dismissLoader();
      });
  }

  private uploadImage(formData, userId) {
    return new Promise((resolve, reject) => {
      this.post(
        "setimage/" + userId + ".json",
        { user_id: userId },
        formData
      ).subscribe(
        response => {
          resolve(response.json());
        },
        err => {
          console.log(err);
          reject(err);
        }
      );
    });
  }
  private post(endpoint: string, params: any = null, body: any = null) {
    return this.http.post('http://dev-trombino.afridev-group.com/api/users/' + endpoint, body, {
      params: params
    });
  }
  private showLoader(message: string = "loading") {
    this.loading = this.loadingCtrl.create({
      content: message,
      // duration: 3000
    });

    this.loading.present();
  }
  private dismissLoader() {
    if (this.loading) {
      this.loading.dismiss();
      this.loading = null;
    }
  }
  presentToast(msg) {
    const toast = this.toastCtrl.create({
      message: msg,
      duration: 3000
    });
    toast.present();
  }

  saveDeviceToken(id, t) {
    // alert(t);
    this.apiService.getRequest('/users/saveregister/' + id + '/' + t + '.json').then(
      (val) => {
        // this.btnMaj = true;
        this.presentToast('Bienvenue');
        console.log("okie");
       },
      (err) => { console.log("echec"); this.presentToast('registre not saved'); }
    ).catch(
      err => { console.log('Catch vaut : ' + err, 'top', 5000);alert(err); }
    )
  }

  saveRegister(id) {
    // to initialize push notifications
    // alert('save register');

    const options: PushOptions = {
      android: {
        senderID: '628604488871',
        vibrate: true,
        sound: true
      },
      ios: {
        alert: 'true',
        badge: true,
        sound: 'false'
      },
      windows: {}
    };

    const pushObject: PushObject = this.push.init(options);
    // alert(pushObject);
    // this.saveDeviceToken(id, 2222);
    pushObject.on('registration').subscribe((registration: any) => {
      // alert('pushObjet token');
      this.saveDeviceToken(id, registration.registrationId);
    }, (err) => {
      // console.log('save register false');
    }
    );


    pushObject.on('error').subscribe(error => console.error('Error with Push plugin', error));
  }

}
