import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { ApiProvider } from '../../providers/api/api';

/**
 * Generated class for the ServicePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

// @IonicPage()
@Component({
  selector: 'page-service',
  templateUrl: 'service.html',
})
export class ServicePage {

  
  service: any;
  listeagent: any;
  liste: any;
  listeAgents = [];
  constructor(public navCtrl: NavController, public navParams: NavParams,
    public apiService: ApiProvider,
    ) {
    this.service = this.navParams.get('value');
    console.log(this.service);
    
    
    // console.log(this.id);
  }
  
  ionViewDidLoad() {
    console.log('ionViewDidLoad ServicePage');
  }
}
