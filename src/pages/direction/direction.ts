import { Component, ViewChild } from '@angular/core';
import { NavController, NavParams, Navbar  } from 'ionic-angular';
import { ApiProvider } from '../../providers/api/api';
import { AgentPage } from '../agent/agent';
import { HomePage } from '../home/home';
import { UserProfilPage } from '../user-profil/user-profil';
import { Storage } from '@ionic/storage';

/**
 * Generated class for the DirectionPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

// @IonicPage()
@Component({
  selector: 'page-direction',
  templateUrl: 'direction.html',
})
export class DirectionPage {
  @ViewChild('navbar') navBar: Navbar;
  direction: any;
  listeservice: any;
  listeagent: any;
  agents: any;
  liste: any;
  listeServices = [];
  dg :any;
  datas = [];
  listeDirecteurs = [];
  Directeurs = [];
  listeAgents = [];
  searchQuery: string = '';
  items:any [];
  arrayAgent = [];
  nb : number = 6;
  urlRecu: any = false;
  urlRecu1: any = false;
  user:any;
  ROOT_IMG : string;
  constructor(public navCtrl: NavController, public navParams: NavParams,
    public apiService: ApiProvider,public store: Storage
    ) {
    this.ROOT_IMG = 'http://dev-trombino.afridev-group.com/img/';
    this.direction = this.navParams.get('value');
    this.store.get('utilisateur').then(
      res => {
        if (res) {
          this.user = res;
          if (this.user.photo != null) {
            this.urlRecu = "http://dev-trombino.afridev-group.com/img/" + this.user.photo;
          }
        }
      }
    );

  }
  ionViewWillEnter() {
    this.store.get('utilisateur').then(
      res => {
        if (res) {
          this.user = res;
          if (this.user.photo != null) {
            this.urlRecu = "http://dev-trombino.afridev-group.com/img/" + this.user.photo
          }
        }
        this.apiService.updateSession(this.user.email)
          .then(
            res => {
              console.log('update session');
              this.store.set('utilisateur', res);
            },
            err => { }
          )
          .catch(
            error => {

            }
          )
      }
    );
  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad ServicePage');
    this.ionViewWillEnter();
    if (this.direction){

      this._getServices(this.direction.entreprise_id);
    }
    else{
      this.navCtrl.setRoot(HomePage);
    }
  }
  goback() {
    this.navCtrl.setRoot(HomePage);
  }
  _getServices(ide){
    this.apiService._showlistservice(ide).then(
      (res)=>{
        this.liste = res;
        this.listeservice = this.liste['services'];
        this.listeagent = this.liste['agents'];
        this.listeDirecteurs = this.liste['directeurs'];
        this.dg = this.liste['dg'];
        this.datas = [];
        this.agents = [];
        this.Directeurs = [];
        //console.log(this.listeDirecteurs);

        this.listeagent.forEach(element => {
          //console.log(this.direction.id);
          //console.log(element.service.direction_id);
          if (this.direction.id == element.service.direction_id) {
            this.agents.push(element);
          }

        });
        //console.log("agents");

        //console.log(this.agents);
        this.listeDirecteurs.forEach(elements => {
          if(this.direction.id === elements.direction_id){

            this.Directeurs.push(elements)
          }
        });
        this.dg.forEach(elements => {
          if(this.direction.id == elements.direction_id){

            this.datas.push({
              id: elements.id,
              nom : elements.nom,
              prenom : elements.prenom,
              poste : elements.poste
            })
          }
        });

        //console.log(this.agents)
        this.listeservice.forEach(element => {
          let ser = [];
          ser =  element.service;
          let obj = [];
          ser.forEach(item => {
            if(this.direction.id == item.direction_id){

              this.listeagent.forEach(element => {
                if(this.direction.id == element.service.direction_id){
                  //console.log('direction id ' + element.service.direction_id);
                  //console.log('direction id ' + this.direction.id);
                  if(item.id == element.service_id){
                      obj.push(element);
                    }
                  }

                });
                item['obj'] = obj;
                this.listeServices.push(item);
              }
           });
          });

        this.initializeItems();
        ////console.log(this.listeServices);

        ////console.log(this.listeAgents);
      },
      (err)=>{
        this.apiService._showlistservice(ide);
      }
    ).catch(
      (error) => {
        this.apiService._showlistedirection(ide).then(res => {
          console.log('try catch');
          this.liste = res;
          this.listeservice = this.liste['services'];
          this.listeagent = this.liste['agents'];
          this.listeDirecteurs = this.liste['directeurs'];

          this.listeagent.forEach(element => {
            if (this.direction.id == element.service.direction_id) {
              console.log("agent found");
              this.agents.push(element);
            }

          });

          this.listeDirecteurs.forEach(elements => {
            if (this.direction.id === elements.direction_id) {

              this.Directeurs.push({
                id: elements.id,
                nom: elements.nom,
                prenom: elements.prenom,
                poste: elements.poste
              })
            }
          });
        }, err => {

        })
      }
    )
  }
  doInfinite(): Promise<any> {
    console.log('Begin async operation');

    return new Promise((resolve) => {
      setTimeout(() => {
        this.nb*=2;
        console.log('Async operation has ended');
        resolve();
      }, 500);
    })
  }
  page() {
    this.navCtrl.push(UserProfilPage);
  }
  viewService(id) {
    ////console.log(id);
    this.navCtrl.push(AgentPage, { 'value': id })
  }
  initializeItems() {
    this.items = [];
    // this.items = this.listeServices;
    this.items = this.agents;

    //console.log(this.items);
    // this.loader.dismiss()
  }
  getItems(ev: any) {
    // Reset items back to all of the items
    this.initializeItems();

    // set val to the value of the searchbar
    const val = ev.target.value;
    // if the value is an empty string don't filter the items
    if (val && val.trim() != '') {
      this.items = this.items.filter((item) => {
        const value = item['nom'] + ' ' + item['prenom'] + ' ' + item['telephone'];
        return (value.toLowerCase().indexOf(val.toLowerCase()) > -1);
      })
    }
  }
}
