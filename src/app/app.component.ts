import { Component, ViewChild } from '@angular/core';
import { Nav, Platform, AlertController, LoadingController, ToastController } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { Push, PushObject,PushOptions } from '@ionic-native/push';
import { HomePage } from '../pages/home/home';
import { ListPage } from '../pages/list/list';
import { DirectionPage } from '../pages/direction/direction';
import { ServicePage } from '../pages/service/service';
import { ApiProvider } from '../providers/api/api';
import { AgentPage } from '../pages/agent/agent';
import { LoginPage } from '../pages/login/login';
import { Storage } from '@ionic/storage';
import { UserEditPage } from '../pages/user-edit/user-edit';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;

  rootPage: any;
  user : any;
  pages: Array<{title: string, component: any}>;
  loading
  constructor(
      public platform: Platform,
      public statusBar: StatusBar,
      public splashScreen: SplashScreen,
      public store : Storage,
      private push: Push,
      private apiService : ApiProvider,
      private alertCtrl : AlertController,
      private loadingCtrl: LoadingController,
      private toastCtrl : ToastController
    ) {
    this.initializeApp();

    // used for an example of ngFor and navigation
    this.pages = [
      { title: 'Home', component: HomePage },
      { title: 'List', component: ListPage },
      { title: 'Agent', component: AgentPage },
      { title: 'Service', component: ServicePage },
      { title: 'Direction', component: DirectionPage },
    ];

  }

  initializeApp() {
    this.splashScreen.hide();
    this.platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      // alert('test');
      this.statusBar.styleDefault();
      this.splashScreen.hide();
      this.store.get('userkey').then(res => {
        if (res != null) {
          this.store.get('active').then(
            data=>{
              if(data != null){
                // this.saveRegister(this.user);
                this.initPushNotification();
                this.rootPage = HomePage;
              }
              else{
                this.store.get('userkey').then(res=>{
                  // alert('verifi user edit');
                  this.rootPage = UserEditPage;
                  this.user = res['id'];
                  console.log(this.user);

                  // console.log("userID appComponent: "+this.user);
                });
              }
            }
          )
        }
        else{
          this.rootPage = LoginPage;
        }
      });


    });
  }
  initPushNotification() {
    // // to check if we have permission
    this.push.hasPermission()
      .then((res: any) => {
        if (res.isEnabled) {
          // alert('We have permission to send push notifications');
        } else {
          // alert('We don\'t have permission to send push notifications');
        }
      });

    // to initialize push notifications

    const options: PushOptions = {
      android: {
        senderID: '628604488871',
        vibrate: true,
        sound: true
      },
      ios: {
        alert: 'true',
        badge: true,
        sound: 'false'
      },
      windows: {}
    };

    const pushObject: PushObject = this.push.init(options);

    pushObject.on('notification').subscribe((notification: any) => {
      // alert('notif :'+notification)
      console.log('Received a notification', notification);

      //Notification Display Section
      let id = notification.additionalData.id;
      let confirmAlert = this.alertCtrl.create({
        title: "Nouveau collaborateur",
        message: notification.message,

        buttons: [{
          text: 'Fermer',
          role: 'cancel'
        }, {
          text: 'consulter',
          handler: () => {
            //TODO: Your logic here
            this.showLoader();
            let body = { title: notification.info.message }
            this.apiService.getRequest("/users/getUser/" + id + ".json").then(
              (post) => {
                this.dismissLoader();
                this.nav.push(AgentPage, { 'value': post })
              },
              (err) => {
                this.dismissLoader();
                this.presentToast('Reject : ' + err, 'top', 3000);
              }
            ).catch(
              err => {
                this.dismissLoader();
                this.presentToast('Catch : ' + err, 'top', 3000);
              }
            )
          }
        }]
      });
      confirmAlert.present();
      //
    });

    pushObject.on('registration').subscribe((registration: any) => {

    });

    pushObject.on('error').subscribe(error => console.error('Error with Push plugin', error));
  }
  private showLoader(message: string = "loading") {
    this.loading = this.loadingCtrl.create({
      content: message,
      duration:5000
    });

    this.loading.present();
  }

  private dismissLoader() {
    if (this.loading) {
      this.loading.dismiss();
      this.loading = null;
    }
  }

  presentToast(msg: string, position: string, delay: number) {
    let toast = this.toastCtrl.create({
      message: msg,
      duration: delay,
      position: position
    });

    toast.onDidDismiss(() => {
      console.log("Dismissed toast");
    });

    toast.present();
  }
  saveDeviceToken(id, t) {
    // alert(t);
    this.apiService.getRequest('/users/saveregister/' + id + '/' + t + '.json').then(
      (val) => { console.log("okie"); },
      (err) => { console.log("echec"); }
    ).catch(
      err => { console.log('Catch vaut : ' + err, 'top', 5000); }
    )
  }

  saveRegister(id) {
    // to initialize push notifications
    // alert('save register');

    const options: PushOptions = {
      android: {
        senderID: '653973277363',
        vibrate: true,
        sound: true
      },
      ios: {
        alert: 'true',
        badge: true,
        sound: 'false'
      },
      windows: {}
    };

    const pushObject: PushObject = this.push.init(options);
    // alert(pushObject);
    // this.saveDeviceToken(id, 2222);
    pushObject.on('registration').subscribe((registration: any) => {
      console.log('pushObjet token');
      this.saveDeviceToken(id, registration.registrationId);
    }, (err) => {
      // console.log('save register false');
    }
    );


    pushObject.on('error').subscribe(error => console.error('Error with Push plugin', error));
  }
  openPage(page) {
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario
    alert('val');
    this.nav.setRoot(page.component);
  }
}
