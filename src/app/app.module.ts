import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { IonicStorageModule } from "@ionic/storage";
import { File} from "@ionic-native/file";
import { Camera } from "@ionic-native/camera";
import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { ListPage } from '../pages/list/list';
import { Push, PushObject, PushOptions } from '@ionic-native/push';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { ApiProvider } from '../providers/api/api';
import { HttpClientModule } from '@angular/common/http';
import { HttpModule } from '@angular/http'
import { DirectionPage } from '../pages/direction/direction';
import { ServicePage } from '../pages/service/service';
import { AgentPage } from '../pages/agent/agent';
import { FormsModule } from "@angular/forms";
import { LoginPage } from '../pages/login/login';
import { UserAuthPage } from '../pages/user-auth/user-auth';
import { UserEditPage } from '../pages/user-edit/user-edit';
import { UserProfilPage } from '../pages/user-profil/user-profil';
import { PapaParseModule } from 'ngx-papaparse';
@NgModule({
  declarations: [
    MyApp,
    HomePage,
    ListPage,
    DirectionPage,
    ServicePage,
    AgentPage,
    LoginPage,
    UserAuthPage,
    UserEditPage,
    UserProfilPage
  ],
  imports: [
    PapaParseModule,
    BrowserModule,
    IonicModule.forRoot(MyApp),
    HttpClientModule,
    HttpModule,
    FormsModule,
    IonicStorageModule.forRoot({
      name: '__mydb',
      driverOrder: ['indexeddb', 'sqlite', 'websql']
    }),

  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    ListPage,
    DirectionPage,
    ServicePage,
    AgentPage,
    LoginPage,
    UserAuthPage,
    UserEditPage,
    UserProfilPage

  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    ApiProvider,
    Camera,
    File,
    Push
  ]
})
export class AppModule {}
