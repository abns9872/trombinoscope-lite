import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import { Storage } from "@ionic/storage";
import "rxjs/add/operator/map"

/*
  Generated class for the ApiProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class ApiProvider {
  apiurl = 'http://dev-trombino.afridev-group.com/api';
  // apiurl = 'http://localhost:82/apicapro/api';

  constructor(public http: Http, private store :  Storage) {
    ////console.log('Hello ApiProvider Provider');
  }
  checkUser(id){
    return new Promise((resolve, reject) => {
      this.http.get(this.apiurl + '/users/checkautorisation/'+id+'.json')
        .subscribe(res => {
          ////console.log(res)
          resolve(res.json());
        }, (err) => {
          ////console.log(this.apiurl)
          reject(err);
        });
    });
  }
  add_Service(array){
    let data = JSON.stringify(array);
    return new Promise((resolve, reject) => {
      this.http.post(this.apiurl + '/add.json',data)
        .subscribe(res => {
          ////console.log(res)
          resolve(res);
        }, (err) => {
          ////console.log(this.apiurl)
          reject(err);
        });
    });
  }
  updateSession(email){
    return new Promise((resolve, reject) => {
      this.http.get(this.apiurl + '/users/updateuser/'+email+'.json')
        .subscribe(res => {
          ////console.log(res)
          resolve(res.json());
        }, (err) => {
          ////console.log(this.apiurl)
          reject(err);
        });
    });
  }
  _showliste() {
    ////console.log("service")
    return new Promise((resolve, reject) => {
      this.http.get(this.apiurl+'/users/index.json')
        .subscribe(res => {
          ////console.log(res.json())
          resolve(res.json());
        }, (err) => {
          reject(err);
        });
    });
  }
  _showlistedirection(entreprise_id) {
    ////console.log("service");
    //console.log(entreprise_id);
    let promise = new Promise((resolve, reject) => {
      this.store.get('ressourcesDirections').then(
        res => {
          if (res != undefined) {
            console.log('Api Storage');
            resolve(JSON.parse(res));
            this.http.get(this.apiurl + '/direction/liste/' + entreprise_id + '.json')
              .subscribe(
                (res) => {
                if (res.status == 200) {
                  // console.log(res.json())
                  this.store.set('ressourcesDirections', JSON.stringify(res.json()));
                  }
                },
                (err)=>{
                  //console.log('erreur getStorage');
                }
              )
          }
          else{
            // return new Promise((resolve, reject) => {
              //console.log("First Requete storage vide");
              this.http.get(this.apiurl + '/direction/liste/' + entreprise_id + '.json')
                .subscribe((res) => {
                  console.log("First Requete storage vide");
                  if (res.status == 200) {
                    console.log(res)
                    resolve(res.json());
                    this.store.set('ressourcesDirections', JSON.stringify(res.json()));
                    let promise = new Promise((resolve, reject) => {
                      this.store.get('ressourcesDirections').then(
                        res => {
                          resolve(JSON.parse(res))
                        },
                        err => {
                          //console.log(err)
                        }
                      )
                    });
                    return promise;
                  }

                },
                  (err) => {
                    //console.log(err)
                  }
                );
            // })
          }
        },
        (err) => {
          //console.log(err)
        }
      )
    });
    return promise;
  }
  _showlistservice(entreprise_id) {
    ////console.log("service");
    // console.log(entreprise_id);
    let promise = new Promise((resolve, reject) => {
      this.store.get('ressources').then(
        res => {
          if (res != null) {
            console.log('Storage non vide');
            resolve(JSON.parse(res));
            this.http.get(this.apiurl + '/direction/listeservice/' + entreprise_id + '.json')
              .subscribe(
                (res) => {
                resolve(res.json());
                if (res.status == 200) {
                  this.store.set('ressources', JSON.stringify(res.json()));
                  resolve(res.json());
                  }
                },
                (err)=>{
                  //console.log();
                }
              )
          }
          else{
            console.log('Storage vide');
            // return new Promise((resolve, reject) => {
              this.http.get(this.apiurl + '/direction/listeservice/' + entreprise_id + '.json')
                .subscribe((res) => {
                  if (res.status == 200) {
                    this.store.set('ressources', JSON.stringify(res.json()));
                    let promise = new Promise((resolve, reject) => {
                      this.store.get('ressources').then(
                        res => {
                          resolve(JSON.parse(res))
                        },
                        err => {
                          //console.log(err)
                        }
                      )
                    });
                    return promise;
                  }
                },
                  (err) => {
                    //console.log(err)
                  }
                );
            // })
          }
        },
        err => {
          //console.log(err)
        }
      )
    });
    return promise;
  }

  serviceListe(entity: string) {
    ////console.log("service")
    let headers = new Headers();
    headers.append('Content-Type', 'application/x-www-form-urlencoded');
    return new Promise((resolve, reject) => {
      this.http.get(this.apiurl+'/'+entity+'/index/json')
        .subscribe(res => {
          ////console.log(res)
          resolve(res);
        }, (err) => {
          ////console.log(this.apiurl)
          reject(err);
        });
    });
  }
  getConnexion(credentiels) {
    let headers = new Headers();

    ////console.log('------Provider------');
    ////console.log(credentiels);
    return new Promise((resolve, reject) => {
      this.http.post(this.apiurl+'/users/auth.json', credentiels, { headers: headers })
        // .map(res=>res.json)
        .subscribe(
          (res) => {
          console.log(res);
            if (res) {
              if (res.status == 200) {
                //console.log('---- status 200 found access a lapplication');
                console.log(res);
                let user = res.json();
                localStorage.setItem('user', JSON.stringify(user));
                resolve(res.json());
              }
              else {
                //console.log('---- status 500 tentative de connexion longue');
                resolve(null)
              }
            }
          else
            ////console.log("erreur daccess a la base de donnees");
            resolve(false);
          },
        (err)=>{
          //console.log(err);
          reject(err);
        }
        );
    },

    );
  }
  requetePost(body) {
    return new Promise((resolve, reject) => {
      this.http.post(this.apiurl+'/users/authbycode/.json',JSON.stringify(body))
        .subscribe(data => {
          //console.log(data);
          resolve(data.json());
        }, (err) => {
          //console.log(err)
          reject(err);
        });
    });
  }
  getRequest(chemin){
    return new Promise((resolve, reject) => {
      this.http.get(this.apiurl + chemin)
        .subscribe(data => {
          resolve(data.json());
        }, (err) => {
          reject(err);
        });
    });
  }
  requeteGet(id, url : string) {
    return new Promise((resolve, reject) => {
      this.http.get(this.apiurl+'/users/'+url+'/'+id+'.json')
        .subscribe(data => {
          resolve(data.json());
        }, (err) => {
          reject(err);
        });
    });
  }
  updateUsers(credentiels){
    let headers = new Headers();
    //console.log('Update');
    //console.log(credentiels)
    return new Promise((resolve, reject) => {
      this.http.post(this.apiurl+'/users/updateutilisateur/'+credentiels.id+'.json', credentiels,{headers:headers})
        .subscribe(data => {
          //console.log(data.json())
          resolve(data.json());
        }, (err) => {
          reject(err);
        });
    });
  }
}
